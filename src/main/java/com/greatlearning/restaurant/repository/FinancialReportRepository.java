package com.greatlearning.restaurant.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.greatlearning.restaurant.entity.FinancialReport;

@Repository
public interface FinancialReportRepository extends JpaRepository<FinancialReport, Integer> {

	@Query(value = "from FinancialReport t where t.purchaseDate >= CURDATE()")
	List<FinancialReport> findTodaysPurchases();
	
}
