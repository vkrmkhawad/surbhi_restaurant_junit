package com.greatlearning.restaurant.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.greatlearning.restaurant.entity.FinancialReport;
import com.greatlearning.restaurant.entity.Item;
import com.greatlearning.restaurant.model.SelectItem;

@Service
public interface RestaurantItemService {

	List<Item> viewItemDetails();
	
	String selectItemDetailsByIds(List<SelectItem> selectItem);
	
	List<FinancialReport> viewFinalBill();
	
	List<FinancialReport> viewDailyReport();
	
	List<FinancialReport> totalMonthlySale();
	
}
