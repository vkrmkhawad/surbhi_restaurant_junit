package com.greatlearning.restaurant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import com.greatlearning.restaurant.entity.User;
import com.greatlearning.restaurant.service.UserService;

@SpringBootTest
@AutoConfigureMockMvc
class UserServiceTest {
	
    @Autowired
    private UserService userService;
    
	@Test
	void shouldBeAbleToFetchUser() throws Exception {
		User user = userService.fetchUser(1);
		assertThat(user.getUserName()).as("admin");
	}
	
	@Test
	void shouldNotBeAbleToFetchUser() throws Exception {
		User user = userService.fetchUser(456);
		assertThat(user).isNull();
	}
	
	@Test
	void shouldBeAbleToRegisterUser() throws Exception {
		User user = new User();
		user.setEmail("vkrmkhawad@abc.com");
		user.setFirstName("vikram");
		user.setLastName("khawad");
		user.setPassword("abc");
		user.setRole("USER");
		user.setUserName("abc");
		String message = userService.createUser(user);
		assertThat(message).as("User is created with role USER");
	}
	
	@Test
	void shouldNotBeAbleToRegisterUser() throws Exception {
		User user = new User();
		user.setEmail("vkrmkhawad@abc.com");
		user.setLastName("khawad");
		user.setPassword("abc");
		user.setRole("USER");
		user.setUserName("abc");
		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			userService.createUser(user);
	    });
		assertThat(exception.getMessage()).contains("not-null property");
	}
	
	@Test
	void shouldBeAbleToUpdateUser() throws Exception {
		User user = new User();
		user.setEmail("abc11@abc.com");
		user.setFirstName("abc11");
		user.setLastName("abc11");
		user.setPassword("$2a$10$wtxBj0Cp1r20yzAyMo5aruyptVDPBXq79oL4goSI3z4X7u0nehZja");
		user.setRole("ADMIN");
		user.setUserName("admin");
		String message = userService.updateUser(user);
		assertThat(message).contains("User updated");
	}
	
	@Test
	void shouldNotBeAbleToUpdateUser() throws Exception {
		User user = new User();
		user.setEmail("abc11@abc.com");
		user.setFirstName("abc11");
		user.setLastName("abc11");
		user.setPassword("abc11");
		user.setRole("USER");
		user.setUserName("abcteststtes");
		String message = userService.updateUser(user);
		assertThat(message).contains("User not found with username "+user.getUserName());
	}
	
	@Test
	void shouldBeAbleToDeleteUser() throws Exception {
		String message = userService.deleteUser(1);
		assertThat(message).as("User deleted");
	}
	
	@Test
	void shouldNotBeAbleToDeleteUser() throws Exception {
		String message = userService.deleteUser(456);
		assertThat(message).isNull();
	}

}
