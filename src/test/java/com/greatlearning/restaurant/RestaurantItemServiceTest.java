package com.greatlearning.restaurant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.greatlearning.restaurant.entity.FinancialReport;
import com.greatlearning.restaurant.entity.Item;
import com.greatlearning.restaurant.model.SelectItem;
import com.greatlearning.restaurant.repository.FinancialReportRepository;
import com.greatlearning.restaurant.repository.ItemDetailsRepository;
import com.greatlearning.restaurant.service.RestaurantItemService;

@SpringBootTest
@AutoConfigureMockMvc
class RestaurantItemServiceTest {
	
    @Autowired
    private RestaurantItemService restaurantService;
    @Mock
    ItemDetailsRepository itemDetailsRepository;
    @Mock
    FinancialReportRepository financialReportRepository;
    
    
    @Test
    public void shouldBeAbleToViewItems() {
    	List<Item> itemList = new ArrayList<>();
    	Item item = new Item();
    	item.setId(1);
    	item.setName("Paneer");
    	item.setPrice(250);
    	itemList.add(item);
    	when(itemDetailsRepository.findAll()).thenReturn(itemList);
    	List<Item> items = restaurantService.viewItemDetails();
		assertThat(items.size() == 1);
    }
    
    @Test
    public void shouldBeAbleToSelectItems() {
    	List<SelectItem> selectedItems = getSelectedItems();
    	Item item = new Item();
    	item.setId(1);
    	item.setName("Paneer");
    	item.setPrice(250);
    	Optional<Item> opt = Optional.of(item);
    	when(itemDetailsRepository.findById(selectedItems.get(0).getItemId())).thenReturn(opt);
    	FinancialReport finReport= new FinancialReport();
    	finReport.setBillId(1);
    	finReport.setItemName("Paneer");
    	when(financialReportRepository.saveAndFlush(finReport)).thenReturn(finReport);
    	String message = restaurantService.selectItemDetailsByIds(selectedItems);
    	assertThat(message).as("Items selected");
    }

	private List<SelectItem> getSelectedItems() {
		List<SelectItem> selectedItems = new ArrayList<>();
    	SelectItem selectItem1 = new SelectItem();
    	selectItem1.setItemId(1);
    	selectItem1.setQty(5);
    	SelectItem selectItem2 = new SelectItem();
    	selectItem2.setItemId(4);
    	selectItem2.setQty(2);
    	selectedItems.add(selectItem1);
    	selectedItems.add(selectItem2);
		return selectedItems;
	}
    
    @Test
	public void shouldBeAbleToViewFinalBill() {
    	List<FinancialReport> finanList = new ArrayList<>();
    	FinancialReport finReport= new FinancialReport();
    	finReport.setBillId(1);
    	finanList.add(finReport);
    	ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("finalcialReportToday", ExampleMatcher.GenericPropertyMatchers.exact())
				.withIgnorePaths("id","username", "itemName", "qty", "price", "purchaseDate");
    	Example<FinancialReport> example = Example.of(finReport, exampleMatcher);
    	
    	when(financialReportRepository.findAll(example)).thenReturn(finanList);
    	List<FinancialReport> finalBill = restaurantService.viewFinalBill();
    	assertThat(finalBill.size() == 1);
    }
    
    @Test
	public void shouldBeAbleToViewDailyReport() {
    	List<FinancialReport> finanList = new ArrayList<>();
    	FinancialReport finReport= new FinancialReport();
    	finReport.setBillId(1);
    	finReport.setItemName("Paneer");
    	finReport.setPurchaseDate(new Date());
    	finanList.add(finReport);
    	when(financialReportRepository.findAll(Sort.by(Direction.DESC, "billId"))).thenReturn(finanList);
    	List<FinancialReport> finalBill = restaurantService.viewDailyReport();
    	assertThat(finalBill.size() == 1);
    }
    
    @Test
	public void shouldBeAbleToViewMonthlyReport() {
    	List<FinancialReport> finanList = new ArrayList<>();
    	FinancialReport finReport= new FinancialReport();
    	finReport.setBillId(1);
    	finReport.setItemName("Paneer");
    	Calendar aCalendar = Calendar.getInstance();
    	// add -1 month to current month
    	aCalendar.add(Calendar.MONTH, -1);
    	finReport.setPurchaseDate(aCalendar.getTime());
    	finanList.add(finReport);
    	when(financialReportRepository.findAll(Sort.by(Direction.DESC, "billId"))).thenReturn(finanList);
    	List<FinancialReport> finalBill = restaurantService.totalMonthlySale();
    	assertThat(finalBill.size() == 1);
    }
    
    
}
